package tp2.nodoExtremo;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.io.*;

public class ClientHilo implements Runnable {

    Socket socketPeer;
    Peer peer;

    public ClientHilo(Socket socketPeer, Peer peer){
        this.socketPeer = socketPeer;
        this.peer = peer;
    }


    @Override
    public void run() {
        try {
            InputStream in= this.socketPeer.getInputStream();
            InputStreamReader inr = new InputStreamReader(in);

            DataInputStream dis;
            BufferedInputStream bis;
            BufferedOutputStream bos;
            int i;
            byte[] byteArray;

            dis = new DataInputStream(this.socketPeer.getInputStream());

            //Recibe el nombre del archivo a enviar
            String fileName= dis.readUTF();
        
            System.out.println(fileName);
            String fileLocation= this.peer.getPath()+fileName;
            in.close();
            inr.close();            

            //Creo un nuevo socket para enviar el archivo
            Socket socket = new Socket("localhost", 9091);          
            final File localFile = new File(fileLocation);
            bis = new BufferedInputStream(new FileInputStream(localFile));
            bos = new BufferedOutputStream(socket.getOutputStream());
             //enviamos el nombre del archivo            
            DataOutputStream dos=new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(localFile.getName());
            //Envio el archivo byte a byte
            byteArray = new byte[8192];
            while ((i = bis.read(byteArray)) != -1){
                bos.write(byteArray,0,i);
            }
            bis.close();
            bos.close();
                       
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
}
