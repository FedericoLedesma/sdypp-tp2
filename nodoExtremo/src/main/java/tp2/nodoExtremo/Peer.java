package tp2.nodoExtremo;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Peer implements Serializable{
    private String address;
    private List<File> archivos;
    private int port;
    private String path;
    private String ip;
    
    public Peer(int port, String path) {
        archivos = new ArrayList<>();
        this.port = port;
        this.path = path;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<File> getArchivos() {
        return this.archivos;
    }

    public void setArchivos(List<File> archivos3) {
        this.archivos = archivos3;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    public void addFile(File file)
    {
        this.archivos.add(file);
    }

    public void agregarArchivo(File archivo){
        this.archivos.add(archivo);
    }
    
    public List<File> getFiles(){
        return this.archivos;
    }


    public String getIp() {
        return ip;
    }

    public void setFile(File file) {
        
    }

}
