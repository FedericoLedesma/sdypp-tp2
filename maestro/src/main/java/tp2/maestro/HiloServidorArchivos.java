package tp2.maestro;


import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class HiloServidorArchivos implements Runnable {

    Socket client;
    Listado listado;

    public HiloServidorArchivos(Socket client, Listado listado) {
        this.client = client;
        this.listado = listado;
    }

    @Override
    public void run() {

        try {
            DataInputStream flujo_entrada = new DataInputStream(client.getInputStream());
            String filename = flujo_entrada.readUTF();
            
            //envio el peer que tenga el archivo

            ObjectOutputStream objeto_salida = new ObjectOutputStream(client.getOutputStream());
            objeto_salida.writeObject(listado.getPeer(filename));
            client.close();
        }catch (Exception e){
            e.printStackTrace();
        }                        
    }

}
