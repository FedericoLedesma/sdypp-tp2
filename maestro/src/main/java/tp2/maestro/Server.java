package tp2.maestro;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.*;

public class Server {
    // Se define el logger
    private final Logger log = LoggerFactory.getLogger(Server.class);
    ServerSocket ss ; 
    Listado listado = new Listado();

    public Server(int port) {
        System.out.println("Server escuchando en: "+port);
        
        try {
            this.ss = new ServerSocket(port);
            log.info ("El servidor se inicio en el puerto:  "+port);
            Socket client;
            BufferedReader br;
            Gson gson = new Gson();

            while (true) {

                client = ss.accept();
                br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter canalSalida = new PrintWriter (client.getOutputStream(), true);

                String solicitudJson = br.readLine();
                System.err.println("Mensaje recibido: "+solicitudJson);

                Message solicitud = gson.fromJson(solicitudJson, Solicitud.class);
                
                //El cleinte solicita el listado de archivos completo de todos los peers
                if (solicitud.getHeader().startsWith("getListadoCompleto")){
                    System.err.println(" Se recibe un GET ");
                    try{
                        
                        //envia la respuesta
                        String respuesta = new Message ("getPerson", gson.toJson(listado.getFiles()));
                        canalSalida.println(respuesta);
                        System.out.println("se envia el siguiente listado: "+respuesta);

                    }catch (Exception e){
                        System.err.println("No se recibio una Solicitud del tipo get "+e.getMessage());
                    }
                }

                //El cliente solicita actualizar el listado de archivos enviando un peer
                if (solicitud.getHeader().startsWith("putListado")){
                    System.err.println(" ES UN PUT ");
                    try{
                        Peer peer = (Peer) gson.fromJson(solicitud.getBodyJSON(), Peer.class);
                        System.err.println("BODY: "+body.getName());
                    }catch (Exception e){
                        System.err.println("NO ES PERSON "+e.getMessage());
                    }

                //El cliente solicita el peer que posee el filename guardado en el body
                //getPeer 

                //posible mejora: dar de baja peers
                    
                   

                }

                

                //  Se hace flush y se registra la respuesta                
                Thread.sleep (5000);
                // // force flush (variable = false)
                pw.flush();
                log.info("Se respodio al puerto cliente: "+client.getPort() );
                
                // // Close current session
                pw.close();
                br.close();
                client.close();

            }

        } catch (Exception e) {
            log.error("msg: "+e.getMessage());
        }

    }

    public static void main( String[] args )
    {
        // [STEP 0] - Configure variables needed in this resource
        
        int port = 9090;
        Server server = new Server(port);
    }
}