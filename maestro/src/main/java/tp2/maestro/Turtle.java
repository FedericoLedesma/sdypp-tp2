package org.example;

public class Turtle {
    String name;
    int id;
    Float size;
    Double weight;

    

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Float getSize() {
        return this.size;
    }

    public void setSize(Float size) {
        this.size = size;
    }

    public Double getWeight() {
        return this.weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }


    public Turtle(String name, int id, Float size, Double weight) {
        this.name = name;
        this.id = id;
        this.size = size;
        this.weight = weight;
    }


    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", id='" + getId() + "'" +
            ", size='" + getSize() + "'" +
            ", weight='" + getWeight() + "'" +
            "}";
    }


}
