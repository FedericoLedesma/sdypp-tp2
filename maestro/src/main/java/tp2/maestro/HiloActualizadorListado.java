package tp2.maestro;

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import tp2.nodoExtremo.Peer;

public class HiloActualizadorListado implements Runnable {
    Listado listado;
    Socket client;

    public HiloActualizadorListado(Socket client, Listado listado) {
        this.listado = listado;
        this.client = client;
    }

    @Override
    public void run() {

        try {
            ObjectInputStream flujo_entrada = new ObjectInputStream(client.getInputStream());
            Peer peer = (Peer) flujo_entrada.readObject();
            listado.add(peer);//actualiza el listado
            
            //envio respuesta con listado actualizado

            ObjectOutputStream objeto_salida = new ObjectOutputStream(client.getOutputStream());
            objeto_salida.writeObject(listado.getFiles());
            client.close();
        }catch (Exception e){
            e.printStackTrace();
        }                
    }

}
