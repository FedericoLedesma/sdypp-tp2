package tp2.maestro;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;

import tp2.nodoExtremo.Peer;

public class Listado {
    private List<Peer> peers;
    // private List<File> archivosTodos; ES calculado

    public Listado() {
        // archivosTodos = new ArrayList<File>();
        peers = new ArrayList<Peer>();

    }

    // agrega a un peer a la lista de peers y agrega los archivos a la lista de
    // archivosTodos
    public boolean add(Peer peer) {
        boolean resultado = true;
        // Funcion para buscar y actualizar la lista un peer de la lista-------------
        int i = 0;
        boolean b = false;
        for (Peer peer3 : peers) {
            if (peer3.getAddress().equals(peer.getAddress()) && (peer3.getPort() == (peer.getPort()))) {
                // actualizo la lista de archivosTodos
                b = true;
                break;
            } else {
                i++;
            }
        }
        if (b) {
            peers.set(i, peer);// TODO mejorar validacion
        } else
            resultado = this.peers.add(peer);

        return resultado;
    }

    // public boolean add(File File) {
    // return this.archivosTodos.add(File);
    // }

    public List<File> getFiles() {
        // TODO ver cuales peers estan activos
        List<File> result = new ArrayList<>();

        for (Peer p : peers) {
            result.addAll(p.getFiles());
        }

        return result;
    }

    public List<File> getFiles(Peer peer) {
        // TODO refactorizar para hacerlo mas seguro y no devolver el array propio
        return peer.getFiles();
    }

    public Peer getFile(String filename) {
        // TODO refactorizar para hacerlo mas seguro y no devolver el array propio
        // Recorremos la las listas de archivos de los peers y elegimos al primero que
        // encontramos
        boolean b = false;
        int i = 0;
        Peer pResultado = null;
        for (Peer p : peers) {
            for (File f : p.getArchivos()) {
                if (f.getName() == filename) {
                    pResultado = p;
                    break;
                } else {
                    i++;
                }
            }
        }
        return pResultado; // Si devuelve nulo es porque no lo encontro
    }

    public List<File> buscar(String regex) {
        List<File> result = new ArrayList<>();
        for (File file : this.getFiles()) {
            if (file.getName().matches(regex)) {
                result.add(file);
            }
        }
        return result;
    }

    public Peer getPeer(String filename) {
        Peer result=null;
        for (Peer peer : peers) {
            for (File file : peer.getFiles()) {
                if (file.getName().matches(filename)) {
                    result = peer;
                    break;//Si lo encuentra deja de buscar
                }
            }
        }
        return result;
    }
}
